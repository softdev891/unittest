/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.wittaya.unitestxo.MainXO;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author AdMiN
 */
public class TDDOXTEST {
    
    public TDDOXTEST() {
    }
 @Test
    public void testCheckOWIN_Vertical1(){
        char table[][] = {{'O', 'X', 'X'},
                          {'O', 'O', 'X'},
                          {'O', 'O','X' }};
        char player = 'O';
        boolean end = true;
            assertEquals(true, MainXO.checkWin());
    }
@Test
    public void testCheckOWIN_Vertical2(){
        char table[][] = {{'O', 'X', 'X'},
                          {'X', 'O', 'X'},
                          {'O', 'O', 'X'}};
        char player = 'O';
        boolean end = true;
            assertEquals(true, MainXO.checkWin());
    }
    @Test
    public void testCheckOWIN_Vertical3(){
        char table[][] = {{'X', 'X', 'O'},
                          {'X', 'O', 'O'},
                          {'X', 'O','O' }};
        char player = 'O';
        boolean end = true;
            assertEquals(true, MainXO.checkWin());
    }
    @Test
    public void testCheckXWIN_Vertical1(){
        char table[][] = {{'X', 'O', 'X'},
                          {'X', 'O', 'X'},
                          {'X', 'X','O' }};
        char player = 'X';
        boolean end = true;
            assertEquals(true, MainXO.checkWin());
    }@Test
    public void testCheckXWIN_Vertical2(){
        char table[][] = {{'O', 'X', 'O'},
                          {'X', 'X', 'O'},
                          {'O', 'X','X' }};
        char player = 'X';
        boolean end = true;
            assertEquals(true, MainXO.checkWin());
    }@Test
    public void testCheckXWIN_Vertical3(){
        char table[][] = {{'X', 'O', 'X'},
                          {'O', 'X', 'X'},
                          {'O', 'O', 'X'}};
        char player = 'X';
        boolean end = true;
            assertEquals(true, MainXO.checkWin());
    }
}
